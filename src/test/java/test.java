import com.codeborne.selenide.SelenideElement;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runners.JUnit4;
import org.openqa.selenium.By;

import static com.codeborne.selenide.CollectionCondition.sizeGreaterThan;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.open;

public class test {
    @Test
    public void marketSamsung() {
        open("https://yandex.ru");
        $(By.xpath(".//a[@data-id='market']")).click();
        //иногда выскакивает капча, для её перехвата при отладке добавлен .waitUntil(visible, 10000)
        //$(By.xpath(".//div[@class='n-w-tabs__horizontal-tabs n-adaptive-layout']")).waitUntil(visible, 10000);
        $(By.xpath(".//a//span[text()='Электроника']")).click();
        $(By.xpath(".//a[text()='Мобильные телефоны']")).click();
        $(By.name("Производитель Samsung")).parent().click();
        $(By.name("Цена от")).val("40000");

        checkFirstElementText();

    }

    @Test
    public void marketBeats() {
        open("https://yandex.ru");
        $(By.xpath(".//a[@data-id='market']")).click();
        //иногда выскакивает капча, для её перехвата при отладке добавлен .waitUntil(visible, 10000)
        //$(By.xpath(".//div[@class='n-w-tabs__horizontal-tabs n-adaptive-layout']")).waitUntil(visible, 10000);
        $(By.xpath(".//a//span[text()='Электроника']")).click();
        $(By.xpath(".//a[text()='Наушники и Bluetooth-гарнитуры']")).click();
        $(By.name("Производитель Beats")).parent().click();
        $(By.name("Цена от")).val("17 000");
        $(By.name("Цена до")).val("25 000");

        checkFirstElementText();


    }

    private void checkFirstElementText(){
        //Можно использовать wait(), но это менее точно
        $(By.xpath(".//*[@class='preloadable__preloader preloadable__preloader_visibility_visible preloadable__paranja']")).should(appear);
        $(By.xpath(".//*[@class='preloadable__preloader preloadable__preloader_visibility_visible preloadable__paranja']")).should(disappears);

        SelenideElement firstProduct = $(By.xpath(".//div[contains(@class, 'col_search-results')]/div/div/div/*[1]/a"));

        String textOnMainPage = firstProduct.getAttribute("title");
        firstProduct.click();

        //можно было завязаться на элемент h1, который и является первым потомком n-title__text
        //но мне кажется, что такой подход более гибкий
        String textInDescription = $(By.xpath(".//div[contains(@class, 'n-title__text')]/*[1]")).getText();

        Assert.assertEquals("Text in description does not match text on main page!" ,textInDescription, textOnMainPage);

    }


}
